<?php
class funcUser {

  static function requireLogin($userType = null) {
    $redirect = false;
    if (!isset($_SESSION[$GLOBALS['app.var.user.username']]) || !isset($GLOBALS['app.var.user.password']) || !isset($GLOBALS['app.user'])) {
      funcCore::redirect(null, 'You are required to login to access this page', $GLOBALS['app.alert.warning']);
    }
    elseif (strtolower(get_class($GLOBALS['app.user'])) != strtolower($GLOBALS['app.var.user.classname'])) {
      funcCore::redirect(null, 'You are required to login to access this page', $GLOBALS['app.alert.warning']);
    }
    elseif (empty($userType)) {
      /**
        if $UserType is empty, allow through
        This means that ANY user type can access this page as long as they're logged in
      **/
    }
    elseif ($GLOBALS['app.user']->Type != $userType) {
      funcCore::redirect(null, 'You do not have permission to access this page', $GLOBALS['app.alert.warning']);
    }
  }
  
  static function loggedIn() {
    if (!empty($_SESSION[$GLOBALS['app.var.user.username']])) {
      return true;
    }
    else {
      return false;
    }
  }
}
?>