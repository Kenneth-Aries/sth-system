<?php
class funcString {
  
  static function br2nl($html) {
    return preg_replace('#<br\s*/?>#i', "\n", $html);
  }
  
  static function getFirstLineWithMatch($haystack, $needle) {
    $lines = explode("\n", $haystack);
    if ($lines) {
      foreach ($lines as $line) {
        if (strpos($line, $needle) !== false) {
          return $line;
        }
      }
    }
    return null;
  }

  static function shorten($str, $len) {
    if (strlen($str) > $len) {
      $str = substr($str, 0, $len) . '...';
    }
    return $str;
  }

  static function toProper($str) {
    return ucwords(strtolower($str));
  }

  static function toSentence($str) {
    return ucfirst(strtolower($str));
  }

  static function filter($str, $filter_type) {
    if (empty($str) || empty($filter_type)) {
      return $str;
    }
    if ($filter_type == 'ALPHANUMERIC') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }
    elseif ($filter_type == 'DIGIT') {
      $allowedChars = '0123456789';
    }
    elseif ($filter_type == 'FILENAME') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._- ()';
    }
    elseif ($filter_type == 'WORDS') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyz- \'';
    }
    else {
      return $str;
    }

    $output = null;
    $i = 0;
    $max = strlen($str);
    for($i=0; $i<$max; $i++) {
      $letter = substr($str, $i, 1);
      if (strpos($allowedChars, $letter) !== false) {
        $output .= $letter;
      }
    }
    return $output;
  }

  static function firstWord($text){
    return substr($text, 0, strpos($text, ' '));
  }

  static function smsParts($text) {
    if (strlen($text) <= 160) {
      return 1;
    }
    else {
      return ceil(strlen($text) / 152);
    }
  }

}
?>