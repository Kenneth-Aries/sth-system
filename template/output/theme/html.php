<?php
if (!isset($title) || empty($title)) {$title = '';} else {$title = ' - '.$title;}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <title><?php echo $GLOBALS['app.title'].$title; ?></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/agency.min.css" />
    <?php echo $cssTop . $styleTop . $jsTop . $scriptTop;?>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </head> 
  <body id="page-top" class="index">
    <?php if(!isset($_GET['ajax']) && $GLOBALS['app.user.loggedin']){
                 
    ?>
    <div id="wrapper">
      <?php echo funcUI::getPage('navigation.php', 'html');?>
      <?php
      //if (funcCore::requireClasses($GLOBALS['app.var.user.classname']) && $GLOBALS['app.user.loggedin']) {
        echo funcUI::getPage('index.php', 'index');
     // }
      ?>
      
      <div id="content">
        <div class="se-pre-con"></div>
        <div class="container">
            <div class="widget-content">
              <?php echo $content; ?>
              <div id="footer" style="color:#B3B3B3;margin-top:20px"> 
            
                <div>mela</div>
              <p style="margin:0">&copy; <?php echo $GLOBALS['app.title'] . ' ' . Date('Y'); ?>. All Rights Reserved.</p>
              <p style="margin:0"><?php //echo session_id();?></p>
            </div>          
            </div>
          </div>
        </div>
        <?php echo $queryStats;?> 
      </div>
    <?php } else{ ?>
    <div class="wrapper">
      <div class="content" style="margin-top: -29px;">
        <div class="container-fluid">
          <div class="row">
             <?php echo $content; ?>
          </div>
          <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <span class="copyright">&copy; <?php echo $GLOBALS['app.title'] . ' ' . Date('Y'); ?>. All Rights Reserved.</span>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
          </footer>  
        </div>
    </div>  
    <?php };?>
            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jqBootstrapValidation.js"></script>
            <script src="js/contact_me.js"></script>
            <script src="js/agency.min.js"></script>
            <script type="text/javascript">
            $(window).load(function() {
              $(".se-pre-con").fadeOut();
            });
            </script>
            <?php echo $cssBottom . $styleBottom . $jsBottom . $scriptBottom; ?> 
  </body>
</html>
