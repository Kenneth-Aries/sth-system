<?php
if (!isset($title) || empty($title)) {$title = '';} else {$title = ' - '.$title;}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <title><?php echo $GLOBALS['app.title'].$title; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/jquery-ui.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/jquery.jscrollpane.css" /> 
    <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/unicorn.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/bootstrap.min.css" />
    <?php echo $cssTop . $styleTop . $jsTop . $scriptTop;?>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!--[if IE 8]>
    <script type="text/css" src="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/ie8.css"></script>
    <![endif]-->
      
  </head> 
  <body data-color="blue" class="flat" style="margin-top: -71px;">
    <?php if(!isset($_GET['ajax']) && $GLOBALS['app.user.loggedin']){
                 
            ?>
    <div id="wrapper">
      <div id="header">
        <a id="menu-trigger" href="#"><i class="fa fa-bars"></i></a>  
      </div>

      <?php echo funcUI::getPage('navigation.php', 'html');?>
      <?php
      //if (funcCore::requireClasses($GLOBALS['app.var.user.classname']) && $GLOBALS['app.user.loggedin']) {
      //  echo funcUI::getPage('form.php', 'login');
     // }
      ?>
      
      <div id="content" style="margin-top: -29px;">
        <div class="se-pre-con"></div>
        <div id="breadcrumb">
         <a href="?module=html&action=html-list"><i class="fa fa-home"></i> Home</a>
          <?php foreach($GLOBALS['breadCrumbMap'] as $breadCrumb){
         //   echo '<a href="' . $breadCrumb["url"] . '">' . $breadCrumb['text'] . '</a>';
          }?>
        </div>
        <div class="container-fluid">
            <div class="widget-content">
              <?php echo $content; ?>
              <div id="footer" style="color:#B3B3B3;margin-top:20px"> 
            <table style="margin:0 auto">
              <tr>
                <td>
                  <span>
                    <a href="http://www.bottomline.co.za/" target="_blank" class="bli tip-top" title="BottomLine Interactive" id="btmLine"></a>
                    <a href="http://www.ccs.co.za/" target="_blank" class="ccs tip-top" title="Customer Care Solutions" id="ccs"></a>
                    <a href="http://www.simdirect.co.za/" target="_blank" class="sim tip-top" title="SimDirect" id="sim"></a>
                  </span>
                </td>
              </tr> 
            </table>
                <div>mela</div>
              <p style="margin:0">&copy; <?php echo $GLOBALS['app.title'] . ' ' . Date('Y'); ?>. All Rights Reserved.</p>
              <p style="margin:0"><?php //echo session_id();?></p>
            </div>          
            </div>
          </div>
        </div>
        <?php echo $queryStats;?> 
      </div>
    <?php } else{ ?>
    <div class="wrapper">
      <div class="content" style="margin-top: -29px;">
        <div class="container-fluid">
          <div class="row">
            <div id="footer" style="color:#B3B3B3;margin-top:20px"> 
            <table style="margin:0 auto">
              <tr>
                <td>
                  <span>
                    <a href="http://www.bottomline.co.za/" target="_blank" class="bli tip-top" title="BottomLine Interactive" id="btmLine"></a>
                    <a href="http://www.ccs.co.za/" target="_blank" class="ccs tip-top" title="Customer Care Solutions" id="ccs"></a>
                    <a href="http://www.simdirect.co.za/" target="_blank" class="sim tip-top" title="SimDirect" id="sim"></a>
                  </span>
                </td>
              </tr> 
            </table> 
            <p>mela</p>
              <p style="margin:0">&copy; <?php echo $GLOBALS['app.title'] . ' ' . Date('Y'); ?>. All Rights Reserved.</p>
            </div>
             <?php echo $content; ?>
          </div>  
        </div>  
      </div>
      <?php echo $queryStats;?>
    </div>  
    <?php };?>
            <script src="js/excanvas.min.js"></script>
            <script src="js/jquery.min.js"></script>
            <script src="js/jquery-ui.custom.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/moment.min.js"></script>
            <script src="js/jquery.sparkline.min.js"></script>
            <script src="js/fullcalendar.min.js"></script>
            <script src="js/jquery.nicescroll.min.js"></script>
            <script src="js/unicorn.js"></script>
            <script type="text/javascript">
            $(window).load(function() {
              $(".se-pre-con").fadeOut();
            });
            </script>
            <?php echo $cssBottom . $styleBottom . $jsBottom . $scriptBottom; ?> 
  </body>
</html>
