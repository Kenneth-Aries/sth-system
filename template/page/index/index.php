<?php
funcCore::requireClasses();

$js =<<<JS
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);
$content .= '<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand page-scroll" href="#page-top">Welcome</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="hidden">
                                <a href="#page-top"></a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#home">Home</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#about">About us</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#catalog">Catalog</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#gallery">Gallery</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#contact-us">Contact us</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#login">Login</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#testimonial">Testimonial</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <header>
            <div class="container">
                <div class="intro-text">
                <div class="intro-lead-in">Welcome to STH Team</div>
                <div class="intro-heading">It is Nice To Meet You</div>
                <a href="#home" class="page-scroll btn btn-xl">See More</a>
            </div>
        </div>
    </header>
    <section id="home">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Images to be select over here</h2>
                    <h3 class="section-subheading text-muted">iframe images and clickable</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg-8">
                    <h4 class="service-heading">Company Profile</h4>
                    <p class="text-muted">Company mission and vision, etc. Information about the company and how they deal with their clients.</p>
                </div>
                <div class="col-md-4">
                    <h4 class="service-heading">Image Catalog</h4>
                    <p class="text-muted">Images catalog goes here</p>
                </div>
                <div class="col-lg-11 text-right">
                    <h4 class="service-heading">Image Centre</h4>
                    <p class="text-muted">Images catalog goes here</p>
                </div>
            </div>
        </div>
    </section>
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About us</h2>
                    <h3 class="section-subheading text-muted">About the company.</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">E-Commerce</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Responsive Design</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Web Security</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
            </div>
        </div>
    </section>
            <section id="catalog" class="bg-light-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading">Catalog</h2>
                            <h3 class="section-subheading text-muted">List of Albums goes here</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 portfolio-item">
                            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content">
                                        <i class="fa fa-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img src="images/catalog/bible-god-quotes-349.jpg" class="img-responsive" alt="">
                            </a>
                            <div class="portfolio-caption">
                                <h4>Weddings</h4>
                                <p class="text-muted">Events</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 portfolio-item">
                            <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content">
                                        <i class="fa fa-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img src="images/catalog/IMG-20140924-WA0001.jpg" class="img-responsive" alt="">
                            </a>
                            <div class="portfolio-caption">
                                <h4>Church</h4>
                                <p class="text-muted">Events</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 portfolio-item">
                            <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content">
                                        <i class="fa fa-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img src="images/catalog/IMG-20140930-WA0003.jpg" class="img-responsive" alt="">
                            </a>
                            <div class="portfolio-caption">
                                <h4>Event</h4>
                                <p class="text-muted">Events</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="gallery" class="bg-light-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading">Gallery</h2>
                            <h3 class="section-subheading text-muted">Gallery Iframe goes here</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 portfolio-item">
                            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content">
                                        <i class="fa fa-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img src="img/portfolio/roundicons.png" class="img-responsive" alt="">
                            </a>
                            <div class="portfolio-caption">
                                <h4>Round Icons</h4>
                                <p class="text-muted">Graphic Design</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 portfolio-item">
                            <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content">
                                        <i class="fa fa-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img src="img/portfolio/startup-framework.png" class="img-responsive" alt="">
                            </a>
                            <div class="portfolio-caption">
                                <h4>Startup Framework</h4>
                                <p class="text-muted">Website Design</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 portfolio-item">
                            <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content">
                                        <i class="fa fa-plus fa-3x"></i>
                                    </div>
                                </div>
                                <img src="img/portfolio/treehouse.png" class="img-responsive" alt="">
                            </a>
                            <div class="portfolio-caption">
                                <h4>Treehouse</h4>
                                <p class="text-muted">Website Design</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Contact Us</h2>
                        <h3 class="section-subheading text-muted">Please fill the form below to contact us.</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form name="sentMessage" id="contactForm" novalidate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        ' . funcForm::text('name', null, 'form-control', null, false, "Placeholder='Full Name'") . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        ' . funcForm::text('email', null, 'form-control', null, false, "Placeholder='Email Address'") . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        ' . funcForm::text('phone', null, 'form-control', null, false, "Placeholder='Contact Number'") .'
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        ' . funcForm::textarea('message', null, 'form-control', null, false, 'style="width:554px;padding-bottom:150px;" Placeholder="Type your message here"') . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-2 text-right">
                                    <div id="success"></div>
                                    ' . funcForm::submit('submit', 'Send Message', 'btn btn-xl', true) . '
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    <section id="login">
            <div class="container">
                <h2>Please fill the form below to register</h2>
                <div class="row">
                <div class="col-md-4">
                    ' . funcForm::validation('formRegistration') 
                      . funcForm::form('formRegistration') . '
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        ' . funcForm::text('txtfullName', null, 'form-control', null, false, "Placeholder='Full Name'") 
                                          . funcForm::text('txtuserName', null, 'form-control', null, false, "Placeholder='Username'") . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        ' . funcForm::text('email', null, 'form-control', null, false, "Placeholder='Email Address'") 
                                          . funcForm::text('txtTelCell', null, 'form-control', null, false, "Placeholder='Cell Number'") . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        ' . funcForm::text('txtPassword', null, 'form-control', null, false, "Placeholder='Password'") 
                                          . funcForm::text('txtConfirmPassword', null, 'form-control', null, false, "Placeholder='Confirm Password'") . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-2 text-right">
                                    <div id="success"></div>
                                    ' . funcForm::submit('btnRegister', 'Register', 'btn btn-primary', true) . '
                                </div>
                            </div>
                        ' . funcForm::closeForm() . '
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    ' . funcForm::validation('formLogin') 
                      . funcForm::form('formLogin') . '
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        ' . funcForm::text('username', null, 'form-control', null, false, "Placeholder='Username'") 
                                        . funcForm::text('password', null, 'form-control', null, false, "Placeholder='Password'") . '
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-2 text-right">
                                    <div id="success"></div>
                                    ' . funcForm::submit('btnLogin', 'Login', 'btn btn-primary', true) . '
                                </div>
                            </div>
                        ' . funcForm::closeForm() . '
                </div>
            </div>
        </div>
    </section>
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>Furniture Spec</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="images/catalog/bible-god-quotes-349.jpg" alt="">
                                <ul>
                                    <li>Spec: Test</li>
                                    <li>Size of equipment: Test</li>
                                    <li>Furniture: Test</li>
                                    <li>Price:</li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times">
                                    </i>Close
                                </button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times">
                                    </i>Add to Car
                                </button>
                                <a href="#" class="btn btn-primary" type="button">Get Qoute</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>Wedding Events</h2>
                                <img class="img-responsive img-centered" src="images/catalog/bible-god-quotes-349.jpg" alt="">
                                <ul>
                                    <li>Spec: Test</li>
                                    <li>Size of equipment: Test</li>
                                    <li>Furniture: Test</li>
                                    <li>Price:</li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times">
                                    </i>Close
                                </button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times">
                                    </i>Add to Car
                                </button>
                                <a href="#" class="btn btn-primary" type="button">Get Qoute</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>Church Pictures</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-cel8ntered" src="images/catalog/bible-god-quotes-349.jpg" alt="">
                                <ul>
                                    <li>Spec: Test</li>
                                    <li>Size of equipment: Test</li>
                                    <li>Furniture: Test</li>
                                    <li>Price:</li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times">
                                    </i>Close
                                </button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times">
                                    </i>Add to Car
                                </button>
                                <a href="#" class="btn btn-primary" type="button">Get Qoute</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
?>