<?php
funcCore::requireClasses('user');  
require_once($GLOBALS['app.folder.include'] . 'class.pagination.php');

$user = User::get(null, null, 'Id ASC');
$countUser = User::count();
                                                        
$content .= '
    <table border="0">
        <tr>
            <td><a class="fg-button ui-state-default ui-corner-all" href="home.php?module=user&action=modify">Add user</a></td>
        </tr>
    </table>
    <hr />
    <h1>' . $countUser . ' users</h1>
    <table id="maintable" border="1" cellspacing="0" cellpadding="2px">
        <tr>
            <th>Action</th>
            <th>Username</th>
            <th>Full Name</th>
            <th>Status</th>
            <th>Type</th>
        </tr>
';

foreach ($user as $u) {
    $content .= '
        <tr>
            <td><a href="home.php?module=user&action=modify&id=' . $u->Id . '">modify</a></td>
            <td>' . $u->Username . '</td>
            <td>' . $u->FullName . '</td>
            <td>' . $u->Status . '</td>
            <td>' . $u->Type . '</td>
        </tr>
    ';
}

$content .= '
    </table>
';

?>