jQuery.noConflict();
function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	
	if(typeof(arr) == 'object') { //Array/Hashes/Objects 
		for(var item in arr) {
			var value = arr[item];
			
			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => '" + value + "'\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}
function urlencode (str) {
  str = (str+'').toString();
  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
                                  replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}
function showAlert(msg, size) {
  if (size == 'small') {
    iHeight = 250;
    iWidth = 400;
  }
  else {
    iHeight = 400;
    iWidth = 600;
  }
  jQuery('#dialog').html(msg);
  jQuery('#dialog').dialog({
    bgiframe: true,
    autoOpen: false,
    modal: true,
    resizable: false,
    draggable: false,
    height: iHeight,
    width: iWidth,
    buttons: {
               Ok: function() {
                   jQuery(this).dialog('close');
               }
             }
  });
  jQuery('#dialog').dialog('open');
}
function clearFormValidation(formId, formCheckElem) {
  jQuery('#' + formId).find('*[class*=validate]').each(function(index, el) {
    formCheckElem.dispose(el);
  });
}
function clearFormValidationElement(formId, formCheckElem, elementId) {
  jQuery('#' + formId).find('*[class*=validate]').each(function(index, el) {
    //do it this way incase an elementId is passed that does not have validation
    if (jQuery(el).attr('id') == elementId) {
      formCheckElem.dispose(el);
    }
  });
}
function requiredIfVisible(el) {
  if (jQuery(el).is(':visible') && jQuery(el).val().trim() == '') {
    //alert('riv: ' + el.id);
  	el.errors.push('This field is required.'); 
  	return false;
  }
  else {
  	return true;
  }
}

function requiredIfPhoneOrEmailFilled(el) {
  if (jQuery(el).val().trim() == '') {
    var name1 = jQuery(el).attr("name");
    var number = name1.charAt(name1.length-1);
    var name2 = 'toPhone' + number;

    if(jQuery('input[name="' + name2 + '"]').length > 0 && jQuery('input[name="' + name2 + '"]').val().trim() != '') {
        el.errors.push('This field is required.');
        return false;
    }
    else {
        var name3 ='toEmail' + number;
        if(jQuery('input[name="' + name3 + '"]').length > 0 && jQuery('input[name="' + name3 + '"]').val().trim() != '') {
          el.errors.push('This field is required.');
          return false;
        }
        else {
          return true;
        }       
    }
  }
  else {
  	return true;
  }
}

function requiredIfNameFilled(el) {
  if (jQuery(el).val().trim() == '') {
    var name1 = jQuery(el).attr("name");
    var number = name1.charAt(name1.length-1);
    var name3 = 'toEmail' + number;
    if(jQuery('input[name="' + name3 + '"]').val().trim() == '') {
      var name2 = 'toName' + number;
      if(jQuery('input[name="' + name2 + '"]').val().trim() != '') {
          el.errors.push('This field is required.');
          return false;
      }
      else {
          return true;       
      }    
    }
    else {
      el.errors.push('This field is required.');
      return true;
    }    
  }
  else {
  	return true;
  }
}

function emailIfVisible(el) {
  var man = true;
  if (jQuery('#targetTelCell').length > 0 && jQuery('#targetTelCell').val() != '') {
    man = false;
  }
  var emailResult = validateEmail(jQuery(el).val(), man);
  if (jQuery(el).is(':visible') && emailResult !== true) {
    //alert('eiv: ' + el.id);
  	el.errors.push(emailResult); 
  	return false;
  }
  else {
  	return true;
  }
}
function landLineIfVisible(el) {
  var landLineResult = validatePhone(jQuery(el).val(), 'ZALandline', true);
  if (jQuery(el).is(':visible') && landLineResult !== true) {
    //alert('eiv: ' + el.id);
  	el.errors.push(landLineResult); 
  	return false;
  }
  else {
  	return true;
  }
}
function cellIfVisible(el) {
  var man = true;
  if (jQuery('#targetEmail').length > 0 && jQuery('#targetEmail').val() != '') {
    man = false;
  }
  var cellResult = validatePhone(jQuery(el).val(), 'ZAMobile', man);
  if (jQuery(el).is(':visible') && cellResult !== true) {
    //alert('eiv: ' + el.id);
  	el.errors.push(cellResult); 
  	return false;
  }
  else {
  	return true;
  }
}

function cellIfVisibleMandatory(el) {
  var result = validatePhone(jQuery(el).val(), 'ZAMobile', true);
  if (jQuery(el).is(':visible') && result !== true) {
    el.errors.push(result);
  	return false;
  }
  else {
  	return true;
  }
}

function validateZALandline(el) {
  jQuery(el).val(jQuery(el).val().trim());
  
  var result = validatePhone(jQuery(el).val(), 'ZALandline', false);
  
  if (result !== true) {
  	el.errors.push(result); 
  	return false;
  }
  else {
  	return true;
  }
}

function validateUKLandline(el) {
  jQuery(el).val(jQuery(el).val().trim());
  
  var result = validatePhone(jQuery(el).val(), 'UKLandline', false);
  
  if (result !== true) {
  	el.errors.push(result); 
  	return false;
  }
  else {
  	return true;
  }
}

function validateZAMobile(el) {
  jQuery(el).val(jQuery(el).val().trim());
  
  var result = validatePhone(jQuery(el).val(), 'ZAMobile', false);
  
  if (result !== true) {
  	el.errors.push(result); 
  	return false;
  }
  else {
  	return true;
  }
}
function validateUKMobile(el) {
  jQuery(el).val(jQuery(el).val().trim());
  
  var result = validatePhone(jQuery(el).val(), 'UKMobile', false);
  
  if (result !== true) {
  	el.errors.push(result); 
  	return false;
  }
  else {
  	return true;
  }
}
function validateAtLeastOneNumber(el) {
  var mobile = jQuery('#customerMobileNumber').val().trim();
  var home = jQuery('#customerHomeNumber').val().trim();
  var work = jQuery('#customerWorkNumber').val().trim();
  if (mobile == '' && home == '' && work == '') {
    el.errors.push('Please provide at least one contact number');
    return false;
  }
  else {
    return true;
  }
}
function validateAtLeastOneNumber2(el) {
  var mobile = jQuery('#targetTelCell').val().trim();
  var work = jQuery('#targetTelLandline').val().trim();
  if (mobile == '' && work == '') {
    el.errors.push('Please provide at least one contact number');
    return false;
  }
  else {
    return true;
  }
}
function validateEmail(addr,man) {
//man = email address is mandatory
if (addr == '' && man) {
   return 'email address is mandatory';
}
if (addr == '') return true;
var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
for (i=0; i<invalidChars.length; i++) {
   if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
      return 'email address contains invalid characters';
   }
}
for (i=0; i<addr.length; i++) {
   if (addr.charCodeAt(i)>127) {
      return 'email address contains non ascii characters.';
   }
}

var atPos = addr.indexOf('@',0);
if (atPos == -1) {
   return 'email address must contain an @';
}
if (atPos == 0) {
   return 'email address must not start with @';
}
if (addr.indexOf('@', atPos + 1) > - 1) {
   return 'email address must contain only one @';
}
if (addr.indexOf('.', atPos) == -1) {
   return 'email address must contain a period in the domain name';
}
if (addr.indexOf('@.',0) != -1) {
   return 'period must not immediately follow @ in email address';
}
if (addr.indexOf('.@',0) != -1){
   return 'period must not immediately precede @ in email address';
}
if (addr.indexOf('..',0) != -1) {
   return 'two periods must not be adjacent in email address';
}
var suffix = addr.substring(addr.lastIndexOf('.')+1);
if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
   return 'invalid primary domain in email address';
}
return true;
}

function validatePhone(Tel, Type, man) {
  if (Tel=='' && !man) {
    return true;
  }
  if (Type=='ZAMobile') {

    //var MobileNumberRegEx = /^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/;
    var MobileNumberRegEx = /^0[678][123456789][0-9]{7}$/;
    if (MobileNumberRegEx.test(Tel)) {
      return true;
    }
    else {
      return "Please enter a mobile number<br />in the following format: 0721234567";
    }
  }
  else if (Type=='UKMobile') {
    
    var MobileNumberRegEx = /^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/;
    //var MobileNumberRegEx = /^0[678][123456789][0-9]{7}/;
    if (MobileNumberRegEx.test(Tel)) {
      return true;
    }
    else {
      return "Please enter a mobile number<br />in the following format: 07201234567";
    }
  }
  else if (Type=='ZALandline') {
    //Landline
    var LandLineRegEx = /^0[123456][0-9]{8}/;
    if (LandLineRegEx.test(Tel)) {
      return true;
    }
    else {
      return "Please enter a valid phone number<br />in the following format: 0211234567";
    }
  }else if(Type == 'UKLandline'){
     //var LandLineRegEx = /^0([123456][0-9]){8}/;
     var LandLineRegEx = /^0(\d ?){10}$/;
    //var MobileNumberRegEx = /^0[678][123456789][0-9]{7}/;
    if (LandLineRegEx.test(Tel)) {
      return true;
    }
    else {
      return "Please enter a landline number<br />in the following format: 023 1234 5678";
    }
  }
  else {
  }
}
var timer = 0;
jQuery(document).ready(function($) {
  timer = window.setTimeout("jQuery('.ui-state-success').parent().hide('slow');", 3000); //hide alerts and errors after 3 seconds
  var doPing = function(sURL, iInterval){$.ajax({url:sURL,complete:function(result, status){if (iInterval!=undefined && iInterval>0){setTimeout(function(){doPing(sURL, iInterval);}, iInterval);}}});};
  doPing('ping.php?ajax', 30000);
  $('#btnToggleWindow').click(function(e) {
    $('#tblContent').toggleClass('compact');
    var uiCompact = 'N';
    if ($('#tblContent').hasClass('compact')) {
      uiCompact = 'Y';
    }
    doPing('set.php?ajax&var=uiCompact&val=' + uiCompact);
  });
});
