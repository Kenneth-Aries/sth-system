 function removeGuest (ele) {
    ele.closest('tr').remove();
}

jQuery(document).ready(function($) {
    $('#btnBack').click(function() { 
        history.back();
    });
    
    $('#date').datetimepicker({ format: 'Y-m-d H:i'});
   // $('#tabs').tabs();
    
    $('#event').change(function() {
      var strEvent = '';
      var strDealer = '';
      strEvent = $('#event option:selected').text();
      strDealer = $('#event option:selected').parent('optgroup').attr('label');
      if (strEvent != 'Please select...') {
        alert('You have selected ' + strDealer + '\'s ' + strEvent);
        $('#eventConfirmation').text('You have selected ' + strDealer + '\'s ' + strEvent);
      }
      else {
        $('#eventConfirmation').text('');
      }
    });
    
    $('#btnCall').click(function() { 
      var input = $('<input>').attr('type', 'hidden').attr('name', 'btnCall').val('call');
      $('#formTarget').append($(input));
      $('#formTarget').submit();        
    });
       
    $('#btnNextCallSave').click(function() { 
      var input = $('<input>').attr('type', 'hidden').attr('name', 'btnNextCallSave').val('save');
      $('#formTarget').append($(input));
      $('#formTarget').submit();
    });
       
    $('#btnNextCallEdit').click(function() { 
      $('#nextContactReadOnly').hide();
      $('#nextContactEdit').show();
    });
    
    $('#btnAddGuest').click(function() {
      var guestTitle = $.trim($("#guestTitle").val());
      var guestFirstName = $.trim($("#guestFirstName").val());
      var guestSurname = $.trim($("#guestSurname").val());
      var guestDietReq = $.trim($("#guestDietReq").val());
      if (!guestFirstName && !guestSurname) {
          alert('Some kind of name is required');
          return false;
      }else if($('#bringPartners').val()){
        if($('.trAddGuest').length > 0){
            alert('sorry, the partner list is full');
            return false;
        }else{
          return true
        } 
      }
      else if ($('#targetBring2PartnersNumber').val()){
        var partnerNumber = $('#targetBring2PartnersNumber').val()
        if($('.trAddGuest').length > partnerNumber - 1){
            alert('Sorry, the partner list is full');
            return false;
        }
      }
      else if($('#eventPartners').val()){
        var partnerNum = $('#eventPartners').val();
        if($('.trAddGuest').length > partnerNum - 1){
            alert('Sorry, the partner list is full');
            return false;
        }
      }

      var n = $("#numberOfGuests").html();
      parseInt(n);
      n--;
      $("#numberOfGuests").html(n);
      $('#tblAdditionalGuests tr:last').before('<tr id="trAddGuest' + n + '" class="trAddGuest" ><td class="guestTitle">' + guestTitle + '</td><td class="guestFirstName">' + guestFirstName + '</td><td class="guestSurname">' + guestSurname + '</td><td class="guestDietReq">' + guestDietReq + '</td><td><img class="pointer imgRemoveGuest" src="template/output/classic_wide/images/cancel.png" width="16px" height="16px" /></td></tr>');
      $('#tblAdditionalGuests .imgRemoveGuest').on("click", function () {
          removeGuest($(this));
      });
      
      $("#guestTitle").val('');
      $("#guestFirstName").val('');
      $("#guestSurname").val('');
      $("#guestDietReq").val('');
    });
    
    $("#btnSubmit, #btnSubmitAddiGuest").click( function () {
        var bringPartnerCheck = $('#bringPartner').is(":checked");
        var rsvpCheck = $('#rsvpY').is(":checked");
        var addiGuestExists = $(".trAddGuest").is(':visible');
        if (!bringPartnerCheck && addiGuestExists) {
            var confirmCheck = confirm('Please be reminded that if the Bring Partner is deselected and then saved, all the additional guests will be removed.');
            if (!confirmCheck) {
                return false;
            }
        }
        
        /**
 * if (!rsvpCheck && addiGuestExists) {
 *             var confirmCheck2 = confirm('Please be reminded that if RSVP Attending is not checked then saved, all the additional guests will be removed.');
 *             if (!confirmCheck2) {
 *                 return false;
 *             }
 *         }
 */
        
        $(".trAddGuest").each( function () {
            var trId = $(this).prop('id');
            var recordId = trId.substr(10);
            
            $('<input>').attr({
                type: 'hidden',
                id: recordId,
                name: 'guestId' + recordId,
                value: recordId
            }).appendTo('#formTarget');
            
            $(this).find('td').each( function () {
                var className = $(this).prop('class');
                var value = $(this).html();
                if (className) {
                    $('<input>').attr({
                        type: 'hidden',
                        id: className + recordId,
                        name: className + recordId,
                        value: value
                    }).appendTo('#formTarget');
                };
            });
        });      
    });
    
    $(".imgRemoveGuest").click( function() {
        removeGuest($(this));
    });

});